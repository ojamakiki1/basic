<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'เกี่ยวกับเรา';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box box-solid box-primary">
  <div class="box-header with-border">
    
    <div class="box-tools pull-right">
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
      
    </div>
    <!-- /.box-tools -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
  
<div class="site-about">
   

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>

  </div>
  <!-- /.box-body -->
  
  <!-- box-footer -->
</div>
<!-- /.box -->



