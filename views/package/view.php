<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Package */





$this->title = $model->pk_name;
$this->params['breadcrumbs'][] = ['label' => 'Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="box box-primary">
  
 
    <div class="box-tools pull-right"  style="margin-right:20px;  margin-top:10px;" >
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
      <span >
      <p>
        <?= Html::a('Update', ['update', 'id' => $model->pk_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pk_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

      </span>
    </div>
    <!-- /.box-tools -->
  
 

  <div class="box-body">
  

 
  
  <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
          [
              'format'=>'raw',
              'attribute'=>'photo',
              'value'=>Html::img($model->photoViewer,['class'=>'img-thumbnail','style'=>'width:200px;'])
          ],
          'pk_id',
          'pk_name',
          'pk_detail',
          'pk_value',
          'pk_number',
          'sys.s_name',
        ],
    ]); 
  ?>

</div>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->