<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var $form yii\widgets\ActiveForm */


?>
 <div class="package-form">
 <?php 
      Modal::begin([
          'header'=>'<h4>package<h4>',
          'id'=>'modal',
          'size'=>'modal-lg',

      ]);
      echo "<div id='modalContent'></div>";

        Modal::end();

   ?>

<div class="box box-primary">
<div class="box-body">


 

<?php $form = ActiveForm::begin([
    'id'=>$model->formName(),
    'options' => ['enctype' => 'multipart/form-data']
    
  ]); ?> 
          <div class="col-md-10">
            <div class="row">
                <div class="col-md-2 " width="200px">
                  <div class="well text-center" >
                <?= Html::img($model->getPhotoViewer(),['style'=>'width:50px;','class'=>'img-rounded']); ?>
                </div>
            </div>
          </div>
    <?= $form->field($model, 'photo')->fileInput() ?>

    <?= $form->field($model, 'pk_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pk_detail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pk_value')->textInput() ?>

    <?= $form->field($model, 'pk_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sys_id')->dropdownList($model->getItemSys()) ?>

    
    


    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
  </div>


</div>



