<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $searchModel app\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="package-index">

<div class="box box-primary">
  



    <div class="box-tools pull-right"  style="margin-right:20px; margin-top:10px;"> 
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
         <p>
        <?= Html::button('Create Package', ['value'=>Url::to('index.php/package/create'),'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>

    

    </div>

  <div class="box-body">


  <?php 
      Modal::begin([
          'header'=>'<h4>package<h4>',
          'id'=>'modal',
          'size'=>'modal-lg',

      ]);
      echo "<div id='modalContent'></div>";

        Modal::end();

   ?>

<?php Pjax::begin(['id' => 'pj_package']) ?>
  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'options'=>['style'=>'width:15s0px;'],
                'format'=>'raw',
                'attribute'=>'photo',
                'value'=>function($model){
                  return Html::tag('div','',[
                    'style'=>'width:150px;height:45px;
                              border-top: 10px solid rgba(255, 255, 255, .46);
                              background-image:url('.$model->photoViewer.');
                              background-size: cover;
                              background-position:center center;
                              background-repeat:no-repeat;
                              ']);
                }
            ],

            'pk_id',
            'pk_name',
            'pk_detail',
            'pk_value',
            'pk_number',
            
           'sys.s_name',
            

            ['class' => 'yii\grid\ActionColumn'],

            
        ],
    ]); ?>
    <?php Pjax::end() ?>
  


    
  
  </div>
  <!-- /.box-body -->

</div>

</div>
