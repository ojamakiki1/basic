<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->


        <!-- search form -->
        <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> --> 
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Package', 'icon' => ' fa-shopping-cart', 'url' => ['/package']],
            
                    ['label' => 'About', 'icon' => ' fa-black-tie', 'url' => ['/site/about']],
                    ['label' => 'Contact', 'icon' => ' fa-send','url' => ['/site/contact']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                   [
//                      'label' => 'Some tools',
//                      'icon' => 'share',
//                      'url' => '#',
//                      'items' => [
//                          ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                          ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                          [
//                              'label' => 'Level One',
//                             'icon' => 'circle-o',
//                              'url' => '#',
//
                            //],
                        //],
                    //],
                ],
            ]
        ) ?>

    </section>

</aside>
